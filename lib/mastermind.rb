require 'byebug'

class Code
  attr_reader :pegs

  PEGS = {
    0=> "R",
    1=> "G",
    2=> "B",
    3=> "Y",
    4=> "O",
    5=> "P"
  }

  def initialize(array)
    validate_pegs(array)
    @pegs = array.map(&:downcase)
  end

  def self.parse(string)
    #validate_pegs(string.chars)
    Code.new(string.chars)
  end

  def validate_pegs(array)
    #puts "#{array}"
    raise ArgumentError, "No array given" if array.nil?
    raise ArgumentError, "incorrect color used" unless array.all? {|ch| 'rgbyop'.include?(ch.downcase)}
    raise ArgumentError, "incorrect number of colors" unless array.length == 4
  end

  def self.random
    Code.new([PEGS[rand(5)],PEGS[rand(5)],PEGS[rand(5)],PEGS[rand(5)]])
  end

  def [](int)
    @pegs[int]
  end

  def exact_matches(code)
    @pegs.each_index.count {|idx| self[idx]==code[idx] }
  end

  def near_matches(code)
    matching_code = code.pegs.dup
    count = 0
    @pegs.each do |peg|
      if matching_code.include?(peg)
        count+=1
        matching_code.delete_at(matching_code.index(peg))
      end
    end
    count - self.exact_matches(code)
  end

  def ==(code)
    return false unless code.class == Code
    self.exact_matches(code) == 4
  end

end

class Game
  attr_reader :secret_code

  def initialize(code=Code.random)
    @secret_code=code
  end

  def get_guess
    ARGV.clear
    begin
      #byebug
      print "Enter guess: "
      guess = gets.chomp
      Code.parse(guess)
    rescue
      puts "Invaid Entry, Try Again"
      retry
    end
  end

  def display_matches(code)
    puts "exact Matches : #{@secret_code.exact_matches(code)}"
    puts "near Matches : #{@secret_code.near_matches(code)}"
  end

  def win?(code)
    @secret_code == code
  end

  def self.play
    game = Game.new
    turns = 0
    won=false
    (0...10).each do |turn|
      turns = turn+1
      puts "\n\nTurn #{turns}: "
      guess_code = game.get_guess
      won = game.win?(guess_code)
      break if won
      game.display_matches(guess_code)
    end

    if won
      puts "Won in #{turns} turns"
    else
      puts "You Lost\nThe Secret code was #{@secret_code}"
    end
  end
end
